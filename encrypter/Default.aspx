﻿<%@ Page Title="Encrypter" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="encrypter._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">Desencripta una cadena de texto utilizando MD5 con una llave que no se revela por motivos de seguridad.</p>
    </div>

    <div class="row">
        <div class="col-md-5">
            <h2>Encriptar</h2>
            <p>AKTIEN LIBRARIES</p>
            <asp:Label ID="lbl1" runat="server" Text="Ingrese texto a encriptar"></asp:Label>
            <br />
            <asp:TextBox ID="txtEncrypter" CssClass="control-form" runat="server" Height="35px" Width="150px"></asp:TextBox>            
            <asp:Button ID="btnEncriptar" CssClass="btn btn-success" runat="server" Text="Encriptar" OnClick="btnEncriptar_Click" />             
            <br />
            <asp:TextBox ID="txtResEncriptar" CssClass="control-form" runat="server" TextMode="MultiLine" Height="40px" Width="300px"></asp:TextBox>       

        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-5">
            <h2>Desencriptar</h2>            
            <p>AKTIEN LIBRARIES </p>
            <asp:Label ID="lbl2" runat="server" Text="Ingrese texto a desencriptar"></asp:Label>
            <br />
            <asp:TextBox ID="txtUnencrypter" CssClass="control-form" runat="server" Height="35px" Width="150px"></asp:TextBox>            
            <asp:Button ID="btnDesencriptar" CssClass="btn btn-danger" runat="server" Text="Desencriptar" OnClick="btnDesencriptar_Click" />
            <br />
            <asp:TextBox ID="txtResDescifrar" CssClass="control-form" runat="server" TextMode="MultiLine" Height="40px" Width="300px"></asp:TextBox>

        </div>
    </div>

</asp:Content>
