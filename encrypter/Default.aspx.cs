﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aktien.Libraries.Utilities;

namespace encrypter
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEncriptar_Click(object sender, EventArgs e)
        {
            String cadena = Convert.ToString(txtEncrypter.Text);
            txtResEncriptar.Text = Utilities.encrypt(cadena);
        }

        protected void btnDesencriptar_Click(object sender, EventArgs e)
        {
            String cadena = Convert.ToString(txtUnencrypter.Text);
            txtResDescifrar.Text = Utilities.decrypt(cadena);

        }
      
    }
}